package ru.andrewkolyanov.demotask.reader;


import java.util.List;
import java.util.Map;

public interface DataReader {

    List<Map<String, Object>> read(String... params);
}
