package ru.andrewkolyanov.demotask.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.andrewkolyanov.demotask.chain.DefaultRuleChain;
import ru.andrewkolyanov.demotask.chain.RuleChain;

import java.io.File;
import java.io.IOException;

public class FileChainReader implements ChainReader {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public RuleChain read(String... params) {
        File chainFile = new File(params[0]);
        try {
            return objectMapper.readValue(chainFile, DefaultRuleChain.class);
        } catch (IOException e) {
            System.out.println("Can't read " + params[0]);
            throw new RuntimeException("Can't read " + params[0], e);
        }
    }
}
