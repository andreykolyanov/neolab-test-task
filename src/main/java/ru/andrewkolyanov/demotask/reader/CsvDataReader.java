package ru.andrewkolyanov.demotask.reader;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvDataReader implements DataReader {

    @Override
    public List<Map<String, Object>> read(String... params) {
        try (Reader reader = new FileReader(params[0])) {
            List<String[]> data = readAll(reader);
            return parse(data);
        } catch (IOException e) {
            System.out.println("Can't read data " + params[0]);
            throw new RuntimeException("Can't read data " + params[0], e);
        }
    }

    private List<String[]> readAll(Reader reader) {
        try (CSVReader csvReader = new CSVReader(reader)) {
            return csvReader.readAll();
        } catch (IOException | CsvException e) {
            System.out.println("Can't read data");
            throw new RuntimeException("Can't read data", e);
        }
    }

    private List<Map<String, Object>> parse(List<String[]> data) {
        String[] header = data.get(0);
        List<String[]> records = data.subList(1, data.size());
        List<Map<String, Object>> result = new ArrayList<>();
        for (String[] row: records) {
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < row.length; i++) {
                map.put(header[i], row[i]);
            }
            result.add(map);
        }
        return result;
    }
}
