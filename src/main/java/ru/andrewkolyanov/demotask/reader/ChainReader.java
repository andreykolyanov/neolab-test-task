package ru.andrewkolyanov.demotask.reader;

import ru.andrewkolyanov.demotask.chain.RuleChain;

public interface ChainReader {

    RuleChain read(String... params);

}
