package ru.andrewkolyanov.demotask.restrictions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AndRestriction extends AbstractRestriction {

    public static final String TYPE = "AND";

    private AbstractRestriction inner1;
    private AbstractRestriction inner2;

    @Override
    public boolean test(Map<String, Object> value) {
        return inner1.test(value) && inner2.test(value);
    }
}
