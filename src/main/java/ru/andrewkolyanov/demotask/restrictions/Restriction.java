package ru.andrewkolyanov.demotask.restrictions;

import java.util.Collection;
import java.util.Map;

public interface Restriction {

    boolean test(Map<String, Object> value);
}
