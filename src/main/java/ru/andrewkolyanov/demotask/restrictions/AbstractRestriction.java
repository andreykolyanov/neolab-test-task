package ru.andrewkolyanov.demotask.restrictions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = AndRestriction.class, name = AndRestriction.TYPE),
        @JsonSubTypes.Type(value = OrRestriction.class, name = OrRestriction.TYPE),
        @JsonSubTypes.Type(value = EqualsRestriction.class, name = EqualsRestriction.TYPE),
        @JsonSubTypes.Type(value = NotEqualsRestriction.class, name = NotEqualsRestriction.TYPE)
})
public abstract class AbstractRestriction implements Restriction {

    private String type;
}
