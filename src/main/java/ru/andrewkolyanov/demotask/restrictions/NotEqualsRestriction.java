package ru.andrewkolyanov.demotask.restrictions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class NotEqualsRestriction extends AbstractRestriction {

    public static final String TYPE = "NE";

    private Object value;
    private String key;

    public boolean test(Map<String, Object> row) {
        return !row.get(key).equals(value);
    }
}
