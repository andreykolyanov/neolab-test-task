package ru.andrewkolyanov.demotask.chain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.andrewkolyanov.demotask.rules.AbstractRule;
import ru.andrewkolyanov.demotask.rules.Rule;

import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
public class DefaultRuleChain implements RuleChain {

    @Setter
    private List<AbstractRule> rules;

    @Override
    public void execute(final Collection<Map<String, Object>> values) {
        Object process = values;
        for (Rule rule: rules) {
            process = rule.execute(process);
        }
    }
}
