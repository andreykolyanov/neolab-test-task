package ru.andrewkolyanov.demotask.chain;

import java.util.Collection;
import java.util.Map;

public interface RuleChain {

    void execute(Collection<Map<String, Object>> values);
}
