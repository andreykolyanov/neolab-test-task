package ru.andrewkolyanov.demotask;

import ru.andrewkolyanov.demotask.chain.RuleChain;
import ru.andrewkolyanov.demotask.reader.ChainReader;
import ru.andrewkolyanov.demotask.reader.CsvDataReader;
import ru.andrewkolyanov.demotask.reader.DataReader;
import ru.andrewkolyanov.demotask.reader.FileChainReader;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        DataReader dataReader = new CsvDataReader();
        ChainReader chainReader = new FileChainReader();

        RuleChain chain = chainReader.read(args[1]);
        List<Map<String, Object>> values = dataReader.read(args[0]);

        chain.execute(values);
    }
}
