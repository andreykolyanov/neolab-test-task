package ru.andrewkolyanov.demotask.rules;

import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@NoArgsConstructor
public class WriteToFileRule extends AbstractRule<Object, Void> {

    @Setter
    private String filename;

    @Setter
    private String pattern;

    @Override
    public Void execute(Object values) {
        File file = new File(filename);
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(String.format(pattern, values));
        } catch (IOException e) {
            System.out.println("Error when try to write file");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Class<?> getInputType() {
        return Object.class;
    }

    @Override
    public Class<?> getOutputType() {
        return Void.class;
    }
}
