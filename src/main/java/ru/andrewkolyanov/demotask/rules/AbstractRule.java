package ru.andrewkolyanov.demotask.rules;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = FilterRule.class, name = "FILTER"),
        @JsonSubTypes.Type(value = CountRule.class, name = "COUNT"),
        @JsonSubTypes.Type(value = WriteToFileRule.class, name = "WRITE_FILE")
})
public abstract class AbstractRule<IN, OUT> implements Rule<IN, OUT> {

    private String type;
}
