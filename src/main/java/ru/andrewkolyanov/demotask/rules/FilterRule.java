package ru.andrewkolyanov.demotask.rules;

import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.andrewkolyanov.demotask.restrictions.AbstractRestriction;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor
public class FilterRule extends AbstractRule<Collection<Map<String, Object>>, Collection<Map<String, Object>>> {

    @Setter
    private AbstractRestriction restriction;

    public Collection<Map<String, Object>> execute(Collection<Map<String, Object>> values) {
        return values.stream().filter(row -> restriction.test(row)).collect(Collectors.toList());
    }

    @Override
    public Class<?> getInputType() {
        return Collection.class;
    }

    @Override
    public Class<?> getOutputType() {
        return Collection.class;
    }
}
