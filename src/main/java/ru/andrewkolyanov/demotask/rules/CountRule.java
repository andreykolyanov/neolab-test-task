package ru.andrewkolyanov.demotask.rules;

import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Map;

@NoArgsConstructor
public class CountRule extends AbstractRule<Collection<Map<String, Object>>, Long> {

    @Override
    public Long execute(Collection<Map<String, Object>> values) {
        return (long) values.size();
    }

    @Override
    public Class<?> getInputType() {
        return Collection.class;
    }

    @Override
    public Class<?> getOutputType() {
        return Long.class;
    }
}
