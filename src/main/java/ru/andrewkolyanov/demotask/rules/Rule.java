package ru.andrewkolyanov.demotask.rules;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface Rule<INPUT, OUTPUT> {

    OUTPUT execute(INPUT values);

    Class<?> getInputType();

    Class<?> getOutputType();
}
