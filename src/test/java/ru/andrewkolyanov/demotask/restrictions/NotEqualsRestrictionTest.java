package ru.andrewkolyanov.demotask.restrictions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NotEqualsRestrictionTest {

    @Test
    void test_true() {
        Restriction restriction = new NotEqualsRestriction("VAL","a");
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertTrue(restriction.test(list), "EqualsRestriction return incorrect result");
    }

    @Test
    void test_false() {
        Restriction restriction = new NotEqualsRestriction("GET","a");
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertFalse(restriction.test(list), "EqualsRestriction return incorrect result");
    }
}