package ru.andrewkolyanov.demotask.restrictions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class EqualsRestrictionTest {

    @Test
    void test_true() {
        Restriction restriction = new EqualsRestriction("VAL","c");
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertTrue(restriction.test(list), "EqualsRestriction return incorrect result");
    }

    @Test
    void test_false() {
        Restriction restriction = new EqualsRestriction("GET","c");
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertFalse(restriction.test(list), "EqualsRestriction return incorrect result");
    }
}