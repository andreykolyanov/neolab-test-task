package ru.andrewkolyanov.demotask.restrictions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrRestrictionTest {

    @Test
    void test_true() {
        Restriction restriction = new OrRestriction(new EqualsRestriction("VAL", "c"), new EqualsRestriction("GET", "a"));
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertTrue(restriction.test(list), "OrRestriction return incorrect result");
    }

    @Test
    void test_false() {
        Restriction restriction = new OrRestriction(new EqualsRestriction("NEW", "a"), new EqualsRestriction("CONST", "b"));
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertFalse(restriction.test(list), "OrRestriction return incorrect result");
    }

    @Test
    void test_true2() {
        Restriction restriction = new OrRestriction(new EqualsRestriction("NEW", "a"), new EqualsRestriction("VAL", "c"));
        Map<String, Object> list = Map.of("a","GET", "b", "SET", "c","VAL");
        assertTrue(restriction.test(list), "OrRestriction return incorrect result");
    }
}