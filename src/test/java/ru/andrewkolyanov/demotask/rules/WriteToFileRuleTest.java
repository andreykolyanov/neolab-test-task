package ru.andrewkolyanov.demotask.rules;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class WriteToFileRuleTest {

    @Test
    void execute() throws IOException {
        WriteToFileRule rule = new WriteToFileRule();
        rule.setPattern("%s");
        rule.setFilename("WriteToFileRuleTest.txt");
        rule.execute("test");
        File file = new File("WriteToFileRuleTest.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String result = reader.readLine();
        assertEquals("test", result, "WriteToFileRule return incorrect result");
    }
}