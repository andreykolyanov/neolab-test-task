package ru.andrewkolyanov.demotask.rules;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CountRuleTest {

    @Test
    void execute() {
        List<Map<String, Object>> values = List.of(
                Map.of("kind", "vegan", "height", "high"),
                Map.of("kind", "all","height", "low"),
                Map.of("kind", "predator","height", "middle")
        );
        CountRule rule = new CountRule();
        Long result = rule.execute(values);
        assertEquals(3L, result, "CountRule Return incorrect result");
    }
}