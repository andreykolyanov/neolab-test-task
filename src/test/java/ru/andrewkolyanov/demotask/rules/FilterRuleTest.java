package ru.andrewkolyanov.demotask.rules;

import org.junit.jupiter.api.Test;
import ru.andrewkolyanov.demotask.restrictions.EqualsRestriction;
import ru.andrewkolyanov.demotask.restrictions.OrRestriction;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FilterRuleTest {

    @Test
    void execute() {
        List<Map<String, Object>> values = List.of(
                Map.of("kind", "vegan", "height", "high"),
                Map.of("kind", "all","height", "low"),
                Map.of("kind", "predator","height", "middle")
        );
        FilterRule rule = new FilterRule();
        rule.setRestriction(new EqualsRestriction("vegan", "kind"));
        Collection<Map<String, Object>> result = rule.execute(values);
        assertEquals(List.of(Map.of("kind", "vegan", "height", "high")), result, "FilterRule return incorrect result");
    }

    @Test
    void execute_or_restrictions() {
        List<Map<String, Object>> values = List.of(
                Map.of("kind", "vegan", "height", "high"),
                Map.of("kind", "all","height", "low"),
                Map.of("kind", "predator","height", "middle")
        );
        FilterRule rule = new FilterRule();
        rule.setRestriction(new OrRestriction(new EqualsRestriction("vegan", "kind"), new EqualsRestriction("low", "height")));
        Collection<Map<String, Object>> result = rule.execute(values);
        assertEquals(List.of(Map.of("kind", "vegan", "height", "high"), Map.of("kind", "all","height", "low")), result, "FilterRule return incorrect result");
    }
}