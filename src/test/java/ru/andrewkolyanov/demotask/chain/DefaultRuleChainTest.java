package ru.andrewkolyanov.demotask.chain;

import org.junit.jupiter.api.Test;
import ru.andrewkolyanov.demotask.restrictions.EqualsRestriction;
import ru.andrewkolyanov.demotask.rules.CountRule;
import ru.andrewkolyanov.demotask.rules.FilterRule;
import ru.andrewkolyanov.demotask.rules.WriteToFileRule;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DefaultRuleChainTest {

    @Test
    void execute() throws IOException {
        List<Map<String, Object>> values = List.of(
                Map.of("kind", "vegan", "height", "high"),
                Map.of("kind", "all","height", "low"),
                Map.of("kind", "predator","height", "middle")
        );
        FilterRule filterRule = new FilterRule();
        filterRule.setRestriction(new EqualsRestriction("vegan", "kind"));
        CountRule countRule = new CountRule();
        WriteToFileRule writeToFileRule = new WriteToFileRule();
        writeToFileRule.setFilename("DefaultRuleChainTest.txt");
        writeToFileRule.setPattern("Count %s");
        RuleChain chain = new DefaultRuleChain(Arrays.asList(filterRule, countRule, writeToFileRule));
        chain.execute(values);
        File file = new File("DefaultRuleChainTest.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String result = reader.readLine();
        assertEquals("Count 1", result, "DefaultChainTest has incorrect result");
    }
}