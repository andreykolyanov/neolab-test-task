package ru.andrewkolyanov.demotask.reader;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CsvDataReaderTest {
    public static final String VALUES = "" +
            "ВЕС,РОСТ,ТИП\n" +
            "ЛЕГКОЕ,МАЛЕНЬКОЕ,ВСЕЯДНОЕ\n" +
            "ТЯЖЕЛОЕ,МАЛЕНЬКОЕ,ТРАВОЯДНОЕ\n" +
            "ТЯЖЕЛОЕ,НЕВЫСОКОЕ,ТРАВОЯДНОЕ";

    @Test
    void read() {
        File testFile = new File("CsvDataReaderTest.txt");
        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(testFile))) {
            osw.append(VALUES);
            osw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DataReader reader = new CsvDataReader();
        List<Map<String, Object>> values = reader.read("CsvDataReaderTest.txt");
        assertNotNull(values);
        try {
            Files.delete(testFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}