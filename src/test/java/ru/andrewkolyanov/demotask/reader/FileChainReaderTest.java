package ru.andrewkolyanov.demotask.reader;

import org.junit.jupiter.api.Test;
import ru.andrewkolyanov.demotask.chain.RuleChain;

import java.io.*;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class FileChainReaderTest {

    public static final String VALUES = "{\n" +
            "  \"rules\": [\n" +
            "    {\n" +
            "      \"type\": \"FILTER\",\n" +
            "      \"restriction\": {\n" +
            "        \"type\": \"EQ\",\n" +
            "        \"key\": \"ТИП\",\n" +
            "        \"value\": \"ВСЕЯДНОЕ\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"type\": \"COUNT\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"type\": \"WRITE_FILE\",\n" +
            "      \"filename\": \"test.txt\",\n" +
            "      \"pattern\": \"%s\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    @Test
    void read() {
        File testFile = new File("FileChainReaderTest.txt");
        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(testFile))) {
            osw.append(VALUES);
            osw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ChainReader reader = new FileChainReader();
        RuleChain chain = reader.read("FileChainReaderTest.txt");
        assertNotNull(chain);
        try {
            Files.delete(testFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}